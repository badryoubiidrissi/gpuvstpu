import torch
import torch_xla
import torch.nn as nn
import model
import torch.utils.data as t_data
import data

import torchtext.datasets as datasets

import torch_xla_py.xla_model as xm
import torch_xla_py.data_parallel as dp

from tqdm import tqdm

class Loader:
    def __iter__(self):
        self.i = 0
        return self
    def __next__(self):
        if self.i < 800:
            self.i +=1
            return (3000*torch.rand(100, 100)).long()
        else:
            raise StopIteration

m = model.RNNModel("LSTM", 30000, 200, 200, 2, 0.5)
corpus = data.Corpus("wikitext-2")
#train_data = t_data.DataLoader(data.Wiki2Dataset(corpus.train, 20, 35), batch_size=None, num_workers = 8)
train_data,_,_ = datasets.WikiText2.iters(batch_size=20, bptt_len = 35, device=-1)
devices = xm.get_xla_supported_devices()


m = dp.DataParallel(m, device_ids=devices, batchdim=1)

def train_loop(model, loader, device, context):
    h = torch.rand(2, 20, 200).to(device), torch.rand(2, 20, 200).to(device)
    t = 0
    pbar = tqdm(desc=f"device : {device}", total=100)
    for _, e in loader:
        t = e.text.to(device)
        out, _ = model(t, h)
        out = out**2
        pbar.update()

for i in tqdm(range(10), desc="Epochs"):
    m(train_loop, train_data)

# import torch
# import torch_xla
# import torch.nn as nn
# import model

# device = 'xla:0'

# m = nn.LSTM(100,100).to(device)
# a = torch.rand(100, 100, 100).to(device)
# h = torch.rand(1, 100, 100).to(device), torch.rand(1, 100, 100).to(device)
# for i in range(100):
#     print(m(a, h))