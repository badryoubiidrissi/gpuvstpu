
<div style="text-align: right"> 09_17_2019_23:12:40 </div>

# Training report
* device : TPU
* batch size : 20
* average ms/batch : 103.08838551363317
* average s/epoch : 47.64803496003151
* Test Loss : 5.487180860929711
* Test PPL : 241.57521187207905

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_TPU_09_17_2019_23:12:40.png) | ![Valid Loss](valid_loss_TPU_09_17_2019_23:12:40.png) | ![Milliseconds per batch](ms_batch_TPU_09_17_2019_23:12:40.png) |