
<div style="text-align: right"> 09_17_2019_22:19:02 </div>

# Training report
* device : TPU
* batch size : 20
* average ms/batch : 103.57710681655394
* average s/epoch : 48.39737220406532
* Test Loss : 5.404737462831099
* Test PPL : 222.4578093731347

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_GPU_09_17_2019_22:19:02.png) | ![Valid Loss](valid_loss_GPU_09_17_2019_22:19:02.png) | ![Milliseconds per batch](ms_batch_GPU_09_17_2019_22:19:02.png) |