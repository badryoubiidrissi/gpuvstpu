
<div style="text-align: right"> 09_18_2019_10:45:04 </div>

# Training report
* device : GPU
* batch size : 20
* average ms/batch : 12.061208162857628
* average s/epoch : 37.79436902999878
* Test Loss : 4.839188527379717
* Test PPL : 126.36676694188353

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_GPU_09_18_2019_10:45:04.png) | ![Valid Loss](valid_loss_GPU_09_18_2019_10:45:04.png) | ![Milliseconds per batch](ms_batch_GPU_09_18_2019_10:45:04.png) |