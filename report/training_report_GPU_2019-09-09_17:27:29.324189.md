<div style="text-align: right"> 2019-09-09 17:27:29.324189 </div>

# Training report
* device : GPU
* batch size : 20
* average ms/batch : 12.00612653366157
* average s/epoch : 37.16047915816307
* Test Loss : 4.677560137818821
* Test PPL : 107.5074489600608


| Train Loss                                                  | Valid Loss                                                     | ms/batch                                                                 |
|-------------------------------------------------------------|----------------------------------------------------------------|--------------------------------------------------------------------------|
| ![Training Loss ](loss_GPU_2019-09-09_17:27:29.324189.png ) | ![Valid Loss ](valid_loss_GPU_2019-09-09_17:27:29.324189.png ) | ![Milliseconds per batch ](ms_batch_GPU_2019-09-09_17:27:29.324189.png ) |