
<div style="text-align: right"> 09_17_2019_00:20:51 </div>

# Training report
* device : TPU
* batch size : 100
* average ms/batch : 427.673978301195
* average s/epoch : 24.40464253425598
* Test Loss : 10.941435515880585
* Test PPL : 56468.34619939514

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
|![Training Loss](loss_GPU_09_17_2019_00:20:51.png)|![Valid Loss](valid_loss_GPU_09_17_2019_00:20:51.png)|![Milliseconds per batch](ms_batch_GPU_09_17_2019_00:20:51.png)|