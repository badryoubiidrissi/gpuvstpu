
<div style="text-align: right"> 09_18_2019_11:19:33 </div>

# Training report
* device : GPU
* batch size : 20
* average ms/batch : 12.03762293394777
* average s/epoch : 36.699758420119416
* Test Loss : 4.984187661579677
* Test PPL : 146.08485647132494

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_GPU_09_18_2019_11:19:33.png) | ![Valid Loss](valid_loss_GPU_09_18_2019_11:19:33.png) | ![Milliseconds per batch](ms_batch_GPU_09_18_2019_11:19:33.png) |