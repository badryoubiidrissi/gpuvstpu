
<div style="text-align: right"> 09_17_2019_20:35:39 </div>

# Training report
* device : TPU
* batch size : 20
* average ms/batch : 263.72739355724593
* average s/epoch : 43.32276970744133
* Test Loss : 5.691642167239354
* Test PPL : 296.37992663923757

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_GPU_09_17_2019_20:35:39.png) | ![Valid Loss](valid_loss_GPU_09_17_2019_20:35:39.png) | ![Milliseconds per batch](ms_batch_GPU_09_17_2019_20:35:39.png) |