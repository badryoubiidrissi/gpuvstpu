
<div style="text-align: right"> 09_18_2019_00:32:16 </div>

# Training report
* device : TPU
* batch size : 20
* average ms/batch : 102.9298216124536
* average s/epoch : 47.56518307030201
* Test Loss : 5.34151716010515
* Test PPL : 208.8292975506191

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_TPU_09_18_2019_00:32:16.png) | ![Valid Loss](valid_loss_TPU_09_18_2019_00:32:16.png) | ![Milliseconds per batch](ms_batch_TPU_09_18_2019_00:32:16.png) |