from datetime import datetime
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import math
import os
import numpy as np


###############################################################################
# Report
###############################################################################

def generate_report(device, batch_size, ms_batch, ave_epoch_time, loss, valid_loss, test_loss):
    if not os.path.exists('report') : os.mkdir('report')
    
    now = datetime.now().strftime("%m_%d_%Y_%H:%M:%S")

    plt.figure()
    plt.plot(ms_batch)
    plt.xlabel("log interval x batch")
    plt.ylabel('average ms per batch')
    plt.savefig(f'report/ms_batch_{device}_{now}.png')

    plt.figure()
    plt.plot(loss)
    plt.xlabel("log interval x batch")
    plt.ylabel('average loss')
    plt.savefig(f'report/loss_{device}_{now}.png')

    plt.figure()
    plt.plot(valid_loss)
    plt.xlabel('epoch')
    plt.ylabel('valid_loss')
    plt.savefig(f'report/valid_loss_{device}_{now}.png')


    with open(f'report/training_report_{device}_{now}.md', 'w') as report_fh:
        report_fh.write(
f'''
<div style="text-align: right"> {now} </div>

# Training report
* device : {device}
* batch size : {batch_size}
* average ms/batch : {np.mean(ms_batch)}
* average s/epoch : {ave_epoch_time}
* Test Loss : {test_loss}
* Test PPL : {math.exp(test_loss)}

| Train Loss                                 | Valid Loss                                   | ms/batch                                               |
|--------------------------------------------|----------------------------------------------|--------------------------------------------------------|
| ![Training Loss](loss_{device}_{now}.png) | ![Valid Loss](valid_loss_{device}_{now}.png) | ![Milliseconds per batch](ms_batch_{device}_{now}.png) |'''
        )