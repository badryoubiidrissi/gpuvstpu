import os
from io import open
import torch
import math
import torch.utils.data as data

class Dictionary(object):
    def __init__(self):
        self.word2idx = {}
        self.idx2word = []

    def add_word(self, word):
        if word not in self.word2idx:
            self.idx2word.append(word)
            self.word2idx[word] = len(self.idx2word) - 1
        return self.word2idx[word]

    def __len__(self):
        return len(self.idx2word)


class Corpus(object):
    def __init__(self, path):
        self.dictionary = Dictionary()
        self.train = self.tokenize(os.path.join(path, 'wiki.train.tokens'))
        self.valid = self.tokenize(os.path.join(path, 'wiki.valid.tokens'))
        self.test = self.tokenize(os.path.join(path, 'wiki.test.tokens'))

    def tokenize(self, path):
        """Tokenizes a text file."""
        assert os.path.exists(path)
        # Add words to the dictionary
        with open(path, 'r', encoding="utf8") as f:
            for line in f:
                words = line.split() + ['<eos>']
                for word in words:
                    self.dictionary.add_word(word)

        # Tokenize file content
        with open(path, 'r', encoding="utf8") as f:
            idss = []
            for line in f:
                words = line.split() + ['<eos>']
                ids = []
                for word in words:
                    ids.append(self.dictionary.word2idx[word])
                idss.append(torch.tensor(ids).type(torch.int64))
            ids = torch.cat(idss)

        return ids

class BPTTIterator:
    def __init__(self, source, bptt, start, end):
        self.source = source
        self.bptt = bptt
        self.start = start
        self.end = end
        self.i = self.start

    def __next__(self):
        if self.i < self.end - self.bptt - 1:
            i = self.i
            data = self.source[i:i+self.bptt]
            target = self.source[i+1:i+1+self.bptt]
            self.i += self.bptt
            return data, target
        else:
            raise StopIteration

class Wiki2Dataset(data.IterableDataset):
    def __init__(self, source, batch_size, bptt):
        super(Wiki2Dataset,self).__init__()
        self.batch_size = batch_size
        self.bptt = bptt
        self.source = self.batchify(source)

    def __iter__(self):
        worker_info = torch.utils.data.get_worker_info()
        if worker_info is None:  # single-process data loading, return the full iterator
            iter_start = 0
            iter_end = self.source.size(0)
        else:  # in a worker process
            # split workload
            per_worker = int(math.ceil((self.source.size(0)) / float(worker_info.num_workers)))
            worker_id = worker_info.id
            iter_start = worker_id * per_worker
            iter_end = min(iter_start + per_worker, self.source.size(0))
        return BPTTIterator(self.source, self.bptt, iter_start, iter_end)

    def batchify(self, data):
        # Work out how cleanly we can divide the dataset into bsz parts.
        nbatch = data.size(0) // self.batch_size
        # Trim off any extra elements that wouldn't cleanly fit (remainders).
        data = data.narrow(0, 0, nbatch * self.batch_size)
        # Evenly divide the data across the self.batch_size batches.
        data = data.view(self.batch_size, -1).t().contiguous()
        return data