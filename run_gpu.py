# coding: utf-8
import argparse
import time
import os
import torch
import torch.nn as nn
import torch.onnx
import numpy as np

import data
import model

import torch
import torch.optim as optim

from report import generate_report

from tqdm import tqdm

import math

parser = argparse.ArgumentParser(description='PyTorch Wikitext-2 RNN/LSTM Language Model')
parser.add_argument('--data', type=str, default='./data/wikitext-2',
                    help='location of the data corpus')
parser.add_argument('--model', type=str, default='LSTM',
                    help='type of recurrent net (RNN_TANH, RNN_RELU, LSTM, GRU, Transformer)')
parser.add_argument('--emsize', type=int, default=200,
                    help='size of word embeddings')
parser.add_argument('--nhid', type=int, default=200,
                    help='number of hidden units per layer')
parser.add_argument('--nlayers', type=int, default=2,
                    help='number of layers')
parser.add_argument('--lr', type=float, default=20,
                    help='initial learning rate')
parser.add_argument('--clip', type=float, default=0.25,
                    help='gradient clipping')
parser.add_argument('--epochs', type=int, default=40,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=20, metavar='N',
                    help='batch size')
parser.add_argument('--bptt', type=int, default=35,
                    help='sequence length')
parser.add_argument('--dropout', type=float, default=0.2,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--tied', action='store_true',
                    help='tie the word embedding and softmax weights')
parser.add_argument('--seed', type=int, default=1111,
                    help='random seed')
parser.add_argument('--device', default=0,
                    help='which device to use')
parser.add_argument('--log-interval', type=int, default=200, metavar='N',
                    help='report interval')
parser.add_argument('--save', type=str, default='model.pt',
                    help='path to save the final model')
parser.add_argument('--onnx-export', type=str, default='',
                    help='path to export the final model in onnx format')

parser.add_argument('--nhead', type=int, default=2,
                    help='the number of heads in the encoder/decoder of the transformer model')

args = parser.parse_args()

# Set the random seed manually for reproducibility.
torch.manual_seed(args.seed)

###############################################################################
# Load data
###############################################################################

tqdm.write("Loading data")

corpus = data.Corpus(args.data)

eval_batch_size = 10

train_data = data.Wiki2Dataset(corpus.train, args.batch_size, args.bptt)
val_data = data.Wiki2Dataset(corpus.valid, eval_batch_size, args.bptt)
test_data = data.Wiki2Dataset(corpus.test, eval_batch_size, args.bptt)

device = torch.device(args.device) if torch.cuda.is_available() else torch.device("cpu") 

###############################################################################
# Build the model
###############################################################################

ntokens = len(corpus.dictionary)
if args.model == 'Transformer':
    model = model.TransformerModel(ntokens, args.emsize, args.nhead, args.nhid, args.nlayers, args.dropout).to(device)
else:
    model = model.RNNModel(args.model, ntokens, args.emsize, args.nhid, args.nlayers, args.dropout, args.tied).to(device)

###############################################################################
# Training code
###############################################################################

def repackage_hidden(h):
    """Wraps hidden states in new Tensors, to detach them from their history."""

    if isinstance(h, torch.Tensor):
        return h.detach()
    else:
        return tuple(repackage_hidden(v) for v in h)

def to(h, device):
    """Wraps hidden states in new Tensors, to detach them from their history."""

    if isinstance(h, torch.Tensor):
        return h.to(device)
    else:
        return tuple(to(v, device) for v in h)


def evaluate_loop_fn(model,loader,device):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    criterion = nn.CrossEntropyLoss()
    total_loss = 0.
    ntokens = len(corpus.dictionary)
    if args.model != 'Transformer':
        hidden = model.init_hidden(eval_batch_size)
        hidden = to(hidden, device)
    with torch.no_grad():
        for i, (data, targets) in enumerate(loader):
            data = data.to(device)
            targets = targets.to(device)
            if args.model == 'Transformer':
                output = model(data)
            else:
                output, hidden = model(data, hidden)
                hidden = repackage_hidden(hidden)
            output_flat = output.view(-1, ntokens)
            targets_flat = targets.view(-1)
            total_loss += len(data) * criterion(output_flat, targets_flat).item()
        print(i)
    return total_loss / (i*args.bptt)

def train_loop_fn(model,loader,device):
    # Turn on training mode which enables dropout.
    model.train()
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=lr)
    total_loss = 0.
    ms_batch_list = []
    loss_list = []
    ntokens = len(corpus.dictionary)

    hidden = model.init_hidden(args.batch_size)
    hidden = to(hidden, device)

    pbar = tqdm(desc=f"Progression on device {device}")
    pbar.reset()

    for i, (data, targets) in enumerate(loader):
        data = data.to(device)
        targets = targets.to(device)
        start_time = time.time()
        # Starting each batch, we detach the hidden state from how it was previously produced.
        # If we didn't, the model would try backpropagating all the way to start of the dataset.
        model.zero_grad()
        if args.model == 'Transformer':
            output = model(data)
        else:
            hidden = repackage_hidden(hidden)
            output, hidden = model(data, hidden)
        output_flat = output.view(-1, ntokens)
        targets_flat = targets.view(-1)
        loss = criterion(output_flat, targets_flat)
        loss.backward()
        #`clip_grad_norm` helps prevent the exploding gradient problem in RNNs / LSTMs.
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.25)

        optimizer.step()
        
        loss_list.append(loss.item())

        elapsed = time.time() - start_time
        ms_batch = elapsed * 1000
        ms_batch_list.append(ms_batch)

        pbar.set_description(desc=f"Progression on device {device} loss {loss.item():.2f}")
        pbar.update()

    return np.array(ms_batch_list), np.array(loss_list)

# Loop over epochs.
lr = args.lr
best_val_loss = None

# At any point you can hit Ctrl + C to break out of training early.
ave_epoch_time = 0
valid_loss = []
loss = []
ms_batch = []
try:
    for epoch in tqdm(range(1, args.epochs+1),desc="Training progress epochs"):
        epoch_start_time = time.time()
        epoch_ms_batch, epoch_loss = train_loop_fn(model, train_data, device)
        loss.append(epoch_loss)
        ms_batch.append(epoch_ms_batch)

        val_loss = evaluate_loop_fn(model, val_data, device)
        valid_loss.append(val_loss)
        epoch_time = time.time() - epoch_start_time
        ave_epoch_time += epoch_time
        tqdm.write('-' * 89)
        tqdm.write('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
                'valid ppl {:8.2f}'.format(epoch, epoch_time,
                                           val_loss, math.exp(val_loss)))
        tqdm.write('-' * 89)
except KeyboardInterrupt:
    tqdm.write('-' * 89)
    tqdm.write('Exiting from training early')

ave_epoch_time /= epoch
loss = np.concatenate(loss)
valid_loss = np.array(valid_loss)
ms_batch = np.concatenate(ms_batch)

# Run on test data.
test_loss = evaluate_loop_fn(model, test_data, device)
tqdm.write('=' * 89)
tqdm.write('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(
    test_loss, math.exp(test_loss)))
tqdm.write('=' * 89)

generate_report('GPU', args.batch_size, ms_batch, ave_epoch_time, loss, valid_loss, test_loss)